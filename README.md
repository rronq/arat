# DISCLAIMER
### This RAT (Remote Administration Tool) is for educational purposes only and should only be used on authorized systems. (Example: Devices that you own) Accessing computer systems without explicit permission or authorization are ILLEGAL.

# ARAT
Another Remote Administration Tool (ARAT) is a simple Python implemenation that lets you setup a remote connection and then run commands on that computer. It is cross-platform and can be easily customizable for different operating systems. 

## Tested Systems
- Windows 10
- Debian 11

## Features
- Changing and listing directories
- Screenshot desktop and upload to Dropbox (Note: You will have to provide a Dropbox API Access Token with the right settings.)
- Encrypt files
- Download files 
- List system information

## Future Work
- Upload files
- Encrypt network communications
- Replace certain hard-coded features

## Requirements
For easy installation, do one of the following:

```
pip install -r requirements.txt
pip3 install -r requirements.txt
```

## Running ARAT
```
#On the remote computer run one of the followintg:
python main.py
python3 main.py
./main.py

```
In case you need executables, Pyinstaller can be utilized. Note that whatever operating system you make it on, it can only be used on that system. 
```
pyinstaller --onefile main.py
#To run it:
main.exe
```

## References
- (https://dev.to/tman540/simple-remote-backdoor-with-python-33a0)
- (https://www.cyborgsecurity.com/cyborg-labs/python-malware-on-the-rise/)
